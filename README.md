# Software Studio 2022 Spring

## Assignment 01 Web Canvas

### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 5%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y         |

---

### How to use

#### Create a canvas

![Create](gif/createCanvas.gif)
<br><br>

#### Tool window

    You can change color, size, font at the bottom of tool window.
![Scroll](gif/scrollToolWindow.gif)
<br><br><br>

### Basic components

#### Basic control tools

![Pen](gif/Pen.gif)
![Eraser](gif/Eraser.gif)
<br><br>

#### Text input

![Text](gif/Text.gif)
<br><br>

#### Cursor icon

    Except for default cursor, there are four types of cursor. 
<img src="mdimg/dragCursor.png" height="50%" width="50%">
<img src="mdimg/penCursor.png" height="50%" width="50%">
<img src="mdimg/eraserCursor.png" height="50%" width="50%">
<img src="mdimg/textCursor.png" height="50%" width="50%">
<br><br>

#### Refresh button

![Refresh](gif/Refresh.gif)
<br><br>

### Advanced tools

#### Different brush shapes

![Shape](gif/Shape.gif)
<br><br>

#### Un/Re-do button

![RedoUndo](gif/RedoUndo.gif)
- Can undo/redo 50 steps at most.
<br><br>

#### Image tool

![Upload](gif/Upload.gif)
<br><br>

#### Download

![Download](gif/Download.gif)
<br><br>

### Other useful widgets

#### Customized canvas size

    You can create canvas with customized width and height (1~9999px).

#### Movable canvas

![Drag](gif/dragCanvas.gif)
<br><br>

### Firebase page link

&nbsp;&nbsp;&nbsp;&nbsp;[Firebase page](https://software-studio-32799.firebaseapp.com/)
